
int synapse_hc595_init (void);
void synapse_hc595_uninit (void);
int synapse_hc595_process (void);

int synapse_hc595_set_off (void);
int synapse_hc595_set_on (void);
int synapse_hc595_get_on (void);
int synapse_hc595_set_text (const char *text);
int synapse_hc595_set_float (float value);
const char * synapse_hc595_get_text (void);
int synapse_hc595_set_gauge (int gauge);
int synapse_hc595_set_indicator1 (int indicator);
int synapse_hc595_get_indicator1 (void);
int synapse_hc595_set_indicator2 (int indicator);
int synapse_hc595_get_indicator2 (void);
int synapse_hc595_set_indicator3 (int indicator);
int synapse_hc595_get_indicator3 (void);
