
#include <stdint.h>
#include <math.h>

#include <common_i2c.h>
#include <common_delay.h>
#include <common_debug.h>

#include "device_sht30.h"

#define SHT30_ADDR              0x44

#define CMD_SHT30_TEMP_NO_HOLD  0xF3
#define CMD_SHT30_HUM_NO_HOLD   0xF5

#define CMD_SHT30_STATUS_H  0xF3
#define CMD_SHT30_STATUS_L  0x2D

#define CMD_SHT30_SINGLE_SHOT_H  0x24
#define CMD_SHT30_SINGLE_SHOT_L  0x0B

/*
 * P(x)=x^8+x^5+x^4+1 = 100110001
 */
const uint16_t POLYNOMIAL = 0x131;

static int check_crc (uint8_t *data, uint8_t nbrOfBytes, uint8_t checksum)
{
        uint8_t crc = 255;
        uint8_t byteCtr;

	//synapse_infof("CRC Checksum : %d", (unsigned int)checksum);

        //calculates 8-Bit checksum with given polynomial
        for (byteCtr = 0; byteCtr < nbrOfBytes; ++byteCtr) {
                crc ^= (data[byteCtr]);
                for (uint8_t bit = 8; bit > 0; --bit) {
                        if (crc & 0x80) crc = (crc << 1) ^ POLYNOMIAL;
                        else crc = (crc << 1);
			//synapse_infof("CRC Value : %d",(unsigned int)crc);
                }
        }
        if (crc == checksum) {
                return 1; // Desired!
        } else {
                return 0; // CRC Error!
        }
}
/*
static int sht30_read_measurement (uint8_t Command,uint16_t *Result)
{
        uint16_t data = 0;
        uint8_t buffer[5];

        buffer[0] = Command;

        if (synapse_i2c_write(SHT30_ADDR, buffer, 1, 1)) {
                synapse_delay_msleep(300);
                if (synapse_i2c_read(SHT30_ADDR, buffer, 3, 1)) {
                        if (!check_crc(buffer, 2, buffer[2])) {
                                return 0;
                        }
                        // clear status bits
                        buffer[1] &= ~0x03;
                        data = (buffer[0]<<8);
                        data += buffer[1];
                        *Result = data;
                } else {
                        return 0;
                }
        } else {
                return 0;
        }
        return 1;
}
*/
int synapse_sht30_init (void)
{
        return 0;
}

void synapse_sht30_uninit (void)
{

}

int synapse_sht30_process (void)
{
        return 0;
}



/*
int synapse_sht30_get_temperature (int32_t *result)
{
        float value;
        uint16_t mbuffer;
        if(sht30_read_measurement(CMD_SHT30_TEMP_NO_HOLD, &mbuffer)) {
                value = -46.85 + mbuffer*(0.00268128);
                if (value < -100 || value > 100) {
                        return -1;
                }
        } else {
                return -1;
        }
        *result = value * 10000000;
        return 0;
}

int synapse_sht30_get_humidity (int32_t *result)
{
        float value;
         uint16_t mbuffer;
        if(sht30_read_measurement(CMD_SHT30_HUM_NO_HOLD, &mbuffer)) {
                value = trunc(-6 + mbuffer * (0.00190735));
                if (value < 0 || value > 102) {
                        return -1;
                }
        } else {
                return -1;
        }
        *result = value * 10000000;
        return 0;
}
*/

int synapse_sht30_get_temperature_and_humidity_single_shot (int32_t *result_temp,int32_t *result_hum)
{
        uint16_t data = 0;
        float value_temp;
	float value_hum;
        uint8_t buffer[6];

        buffer[0] = CMD_SHT30_SINGLE_SHOT_H;
        buffer[1] = CMD_SHT30_SINGLE_SHOT_L;

	if (synapse_i2c_write(SHT30_ADDR, buffer, 2, 1)) {
                synapse_delay_msleep(300);
                if (synapse_i2c_read(SHT30_ADDR, buffer, 6, 1)) {
			//No crc check!    	
			// temp ...
                        data = (buffer[0]<<8);
                        data += buffer[1];
			value_temp = -45 + data*(0.00267032);
		        if (value_temp < -100 || value_temp > 100) {
                	        return -1;
                	}
			*result_temp = value_temp * 10000000;

			// humi ...
                        data = (buffer[3]<<8);
                        data += buffer[4];
	                value_hum = trunc(data * (0.00152590));
	                if (value_hum < 0 || value_hum > 102) {
	                        return -1;
	                }
        		*result_hum = value_hum * 10000000;

                } else {
			synapse_infof("Zero 0!");
                        return 0;
                }
	} else {
		synapse_infof("Zero 1!");
                return 0;
        }

        return 1;

}

int sht30_read_status (uint16_t *Result)
{
        uint16_t data = 0;
        uint8_t buffer[5];

        buffer[0] = CMD_SHT30_STATUS_H;
        buffer[1] = CMD_SHT30_STATUS_L;

        if (synapse_i2c_write(SHT30_ADDR, buffer, 2, 1)) {
                synapse_delay_msleep(300);
                if (synapse_i2c_read(SHT30_ADDR, buffer, 3, 1)) {
                        if (!check_crc(buffer, 2, buffer[2])) {
				//synapse_infof("Zero 1!");
                                return 0;
                        }
                        // clear status bits
                        //buffer[1] &= ~0x03;
                        data = (buffer[0]<<8);
                        data += buffer[1];
                        *Result = data;
                } else {
			//synapse_infof("Zero 2!");
                        return 0;
                }
        } else {
		//synapse_infof("Zero 3!");
                return 0;
        }
        return 1;
}
