
#include <string.h>
#include <stdint.h>
#include <math.h>

#include <asf.h>
#include <common_pinmux.h>
#include <common_delay.h>

#include "device_hc595.h"

static int g_on;
static int g_dirty;

static int g_gauge;
static int g_indicator1;
static int g_indicator2;
static int g_indicator3;
static char g_text[7];

/*
 *    0
 *  5   1
 *    6
 *  4   2
 *    3   7
 */
static void convert_text_to_7segment_bytes (const char *input, char *output, uint8_t output_length)
{
        uint8_t writecursor = 0;
        uint8_t readcursor = 0;
        uint8_t inputlength = strlen(input);

        while (readcursor < inputlength && writecursor < output_length) {
                if (input[readcursor] == '.' && writecursor > 0) {
                        output[writecursor - 1] &= 0b01111111;
                } else {
                        switch (input[readcursor]) {
                                case '.':
                                        output[writecursor] = 0b10000000;
                                        break;
                                case '0':
                                        output[writecursor] = 0b00111111;
                                        break;
                                case '1':
                                        output[writecursor] = 0b00000110;
                                        break;
                                case '2':
                                        output[writecursor] = 0b11011011;
                                        break;
                                case '3':
                                        output[writecursor] = 0b01001111;
                                        break;
                                case '4':
                                        output[writecursor] = 0b01100110;
                                        break;
                                case '5':
                                        output[writecursor] = 0b01101101;
                                        break;
                                case '6':
                                        output[writecursor] = 0b01111101;
                                        break;
                                case '7':
                                        output[writecursor] = 0b00000111;
                                        break;
                                case '8':
                                        output[writecursor] = 0b01111111;
                                        break;
                                case '9':
                                        output[writecursor] = 0b01101111;
                                        break;
                                case 'A':
                                        output[writecursor] = 0b01110111;
                                        break;
                                case 'B':
                                        output[writecursor] = 0b01111100;
                                        break;
                                case 'C':
                                        output[writecursor] = 0b00111001;
                                        break;
                                case 'D':
                                        output[writecursor] = 0b01011110;
                                        break;
                                case 'E':
                                        output[writecursor] = 0b01111001;
                                        break;
                                case 'F':
                                        output[writecursor] = 0b01110001;
                                        break;
                                case 'L':
                                        output[writecursor] = 0b00111000;
                                        break;
                                case 'P':
                                        output[writecursor] = 0b01110011;
                                        break;
                                case '-':
                                        output[writecursor] = 0b01000000;
                                        break;
                                default:
                                        output[writecursor] = 0b00000000;
                                        break;
                        }
                        writecursor++;
                }

                readcursor++;
        }

        while (writecursor < output_length) {
                output[writecursor] = 0b11111111;
                writecursor++;
        }
}

static void hc595_gpio_setup (uint32_t dpin, uint32_t shpin, uint32_t stpin)
{
        synapse_pinmux_setup(dpin, 1, 0, 0);
        synapse_pinmux_setup(shpin, 1, 0, 0);
        synapse_pinmux_setup(stpin, 1, 0, 0);
}

static void hc595_pulse_pin (uint32_t pin)
{
        synapse_pinmux_set_level(pin, 1);
        synapse_delay_usleep(10);

        synapse_pinmux_set_level(pin, 0);
        synapse_delay_usleep(10);
}

static void hc595_gpio_push_byte (uint8_t data, uint32_t dpin,uint32_t shpin, uint32_t stpin)
{
        uint8_t i;
        // shift data and clock the MSB into HC595
        for (i = 0; i < 8; i++) {
                if(data & 0b10000000) {
                        synapse_pinmux_set_level(dpin, 1);
                } else {
                        synapse_pinmux_set_level(dpin, 0);
                }
                // Pulse the Clock line
                hc595_pulse_pin(shpin);
                // Now bring next bit at MSB position
                data = data << 1;
        }
}


static void hc595_gpio_latch_out (uint32_t dpin, uint32_t shpin, uint32_t stpin)
{
        hc595_pulse_pin(stpin);
}

static void hc595_push_text (const char* text)
{
        int i;
        char display_bytes[10];
	// Buradaki 2'ler 3 idi.
        convert_text_to_7segment_bytes(text, display_bytes, 2);
        for (i = 0; i < 2; i++) {
                hc595_gpio_push_byte(display_bytes[i], PIN_HC595_DATA, PIN_HC595_CLK, PIN_HC595_LATCH);
        }
}

int synapse_hc595_init (void)
{
        g_gauge      = 0;
        g_indicator1 = 0;
        g_indicator2 = 0;
        g_indicator3 = 0;
        g_text[0]    = 0;

        hc595_gpio_setup(PIN_HC595_DATA, PIN_HC595_CLK, PIN_HC595_LATCH);
        synapse_hc595_set_off();
        g_dirty = 0;

        return 0;
}

void synapse_hc595_uninit (void)
{

}

int synapse_hc595_process (void)
{
        if (g_on != 0 &&
            g_dirty != 0) {
                //uint8_t bits;
                hc595_gpio_setup(PIN_HC595_DATA, PIN_HC595_CLK, PIN_HC595_LATCH);
                //bits = ~(0 | (g_gauge > 0) * 8 | (g_gauge > 1) * 4 | (g_gauge > 2) * 2 | (g_gauge > 3) * 1 | (g_indicator1 != 0) * 16 | (g_indicator2 != 0) * 32 | (g_indicator3 != 0) * 64);
                //hc595_gpio_push_byte(bits, PIN_HC595_DATA, PIN_HC595_CLK, PIN_HC595_LATCH);
                hc595_push_text(g_text);
                hc595_gpio_latch_out(PIN_HC595_DATA, PIN_HC595_CLK, PIN_HC595_LATCH);
        }
        g_dirty = 0;
        return 0;
}

int synapse_hc595_set_off (void)
{
        g_on = 0;
        hc595_gpio_push_byte(0xFF, PIN_HC595_DATA,PIN_HC595_CLK, PIN_HC595_LATCH);
        hc595_gpio_push_byte(0xFF, PIN_HC595_DATA,PIN_HC595_CLK, PIN_HC595_LATCH);
        hc595_gpio_push_byte(0xFF, PIN_HC595_DATA,PIN_HC595_CLK, PIN_HC595_LATCH);
        hc595_gpio_push_byte(0xFF, PIN_HC595_DATA,PIN_HC595_CLK, PIN_HC595_LATCH);
        hc595_gpio_latch_out(PIN_HC595_DATA, PIN_HC595_CLK, PIN_HC595_LATCH);
        return 0;
}

int synapse_hc595_set_on (void)
{
        g_on = 1;
        return 0;
}

int synapse_hc595_get_on (void)
{
        return g_on;
}

int synapse_hc595_set_text (const char *text)
{
        if (text == NULL) {
                text = "";
        }
        snprintf(g_text, sizeof(g_text), "%s", text);
        g_dirty = 1;
        return 0;
}

int synapse_hc595_set_float (float value)
{
        int negative;
        int tens;
        int ones;
        int fraction;

        if (value<0) {
            value = -value;
            negative = true;
        } else {
            negative = false;
        }

        tens = trunc(value / 10);
        ones = ((int) value) % 10;
        fraction = trunc(value * 10) - trunc(value) * 10;

        memset(g_text, 0, sizeof(g_text));

        if (negative == false) {
                g_text[0] = tens != 0 ? (tens  + '0') : ' ';
                g_text[1] = ones + '0';
                g_text[2] = '.';
                g_text[3] = fraction  + '0';
        } else {
                g_text[0] = '-';
            if (tens == 0) {
                    g_text[1] = ones + '0';
                    g_text[2] = '.';
                    g_text[3] = fraction  + '0';
            } else {
                    g_text[1] = tens + '0';
                    g_text[2] = ones + '0';
            }
        }
        g_dirty = 1;
        return 0;
}

const char * synapse_hc595_get_text (void)
{
        return g_text;
}

int synapse_hc595_set_gauge (int gauge)
{
        g_gauge = gauge;
        g_dirty = 1;
        return 0;
}

int synapse_hc595_set_indicator1 (int indicator)
{
        g_indicator1 = indicator;
        g_dirty = 1;
        return 0;
}

int synapse_hc595_get_indicator1 (void)
{
        return g_indicator1;
}

int synapse_hc595_set_indicator2 (int indicator)
{
        g_indicator2 = indicator;
        g_dirty = 1;
        return 0;
}

int synapse_hc595_get_indicator2 (void)
{
        return g_indicator2;
}

int synapse_hc595_set_indicator3 (int indicator)
{
        g_indicator3 = indicator;
        g_dirty = 1;
        return 0;
}

int synapse_hc595_get_indicator3 (void)
{
        return g_indicator3;
}
