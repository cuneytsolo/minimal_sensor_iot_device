
enum  {
        SYNAPSE_WIFI_STATE_ATOK	                 = 0,
	SYNAPSE_WIFI_STATE_INITIAL		 = 1,
	SYNAPSE_WIFI_STATE_CONNECT		 = 2,
	#define SYNAPSE_WIFI_STATE_ATOK					 SYNAPSE_WIFI_STATE_ATOK
	#define SYNAPSE_WIFI_STATE_INITIAL				 SYNAPSE_WIFI_STATE_INITIAL
	#define SYNAPSE_WIFI_STATE_CONNECT				 SYNAPSE_WIFI_STATE_CONNECT
};

enum {
        SYNAPSE_WIFI_STATE_STATUS_RUNNING        = 0,
        SYNAPSE_WIFI_STATE_STATUS_SUCCESS        = 1,
        SYNAPSE_WIFI_STATE_STATUS_ERROR          = 2,
#define SYNAPSE_WIFI_STATE_STATUS_RUNNING        SYNAPSE_WIFI_STATE_STATUS_RUNNING
#define SYNAPSE_WIFI_STATE_STATUS_SUCCESS        SYNAPSE_WIFI_STATE_STATUS_SUCCESS
#define SYNAPSE_WIFI_STATE_STATUS_ERROR          SYNAPSE_WIFI_STATE_STATUS_ERROR
};

int synapse_wifi_init(void);
int synapse_wifi_process(void);
void synapse_wifi_uninit(void);

int synapse_wifi_atok (void);
int synapse_wifi_initial (void);
int synapse_wifi_connect (void);

const char * wifi_state_string (void);

unsigned int synapse_wifi_get_state (void);
unsigned int synapse_wifi_get_state_status (void);

void buffer_copier_caller(void (*emitter)(char *buffer));
