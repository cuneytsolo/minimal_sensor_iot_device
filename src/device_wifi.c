
#include <asf.h>

#include "common.h"
#include "common_debug.h"
#include "common_pinmux.h"
#include "common_time.h"
#include "device_wifi.h"
#include "common_delay.h"

#include <stdarg.h>
#include <sys/time.h>
#include <inttypes.h>
#include <string.h>

//char try_http_get[]="GET / HTTP/1.1\r\nHost: 34.67.16.50:12345\r\n\r\n"; //cipsend = 43 WORKS!

//char try_http_get[]="POST / HTTP/1.1\r\nHost: 34.67.16.50\r\nContent-Type: application/json\r\nContent-Length:11\r\n\r\n{\"c\": \"55\"}\r\n\r\n"; //cipsend = strlen(try_http_get) WORKS !

//char try_http_get[]="POST / HTTP/1.1\r\nHost: 34.67.16.50\r\nContent-Type: application/json\r\nContent-Length:54\r\n\r\n{\"count\": \"55\", \"temperature\": \"55\", \"humidity\": \"55\"}\r\n\r\n"; //cipsend = strlen(try_http_get) WORKS !

struct wifi_step {
        const char *name;
        int (*process) (void);
};

struct wifi_operation {
        const char *name;
        struct wifi_step **steps;
        unsigned int nsteps;
};

struct wifi_state {
        const char *name;
        struct wifi_operation **operations;
        unsigned int noperations;
};

static unsigned int g_wifi_state;
static unsigned int g_wifi_state_status;
static unsigned int g_wifi_operation;
static unsigned int g_wifi_step;
static unsigned int g_wifi_pstep;
static struct timeval g_wifi_state_timeval;
static struct timeval g_wifi_operation_timeval;
static struct timeval g_wifi_step_timeval;

static struct usart_module g_wifi_uart_module;

int lease_width;
#define SYNAPSE_WIFI_LEASE_TIME_BUFFER_SIZE            25
#define SYNAPSE_WIFI_IP_RANGE_BUFFER_SIZE              35
#define SYNAPSE_WIFI_STA_MAC_BUFFER_SIZE               20
#define SYNAPSE_WIFI_STA_IP_BUFFER_SIZE				   20
//static char g_wifi_lease_time[SYNAPSE_WIFI_LEASE_TIME_BUFFER_SIZE];
//static char g_wifi_ip_range[SYNAPSE_WIFI_IP_RANGE_BUFFER_SIZE];
//static char g_wifi_sta_mac[SYNAPSE_WIFI_STA_MAC_BUFFER_SIZE];
//static char g_wifi_sta_ip[SYNAPSE_WIFI_STA_IP_BUFFER_SIZE];

#define SYNAPSE_WIFI_INPUT_BUFFER_SIZE           (2 * 1024)
static uint8_t g_wifi_input_buffer[SYNAPSE_WIFI_INPUT_BUFFER_SIZE];

#define SYNAPSE_WIFI_OUTPUT_BUFFER_SIZE          (512)
char g_wifi_output_buffer[SYNAPSE_WIFI_OUTPUT_BUFFER_SIZE];

#define SYNAPSE_WIFI_SEND_BUFFER_SIZE          (1024)
char g_wifi_send_buffer[SYNAPSE_WIFI_SEND_BUFFER_SIZE];

static void (*g_wifi_state_tcp_request_emitter_callback) (char *buffer);

static ssize_t wifi_write_wait (const void *data, size_t count)
{
        enum status_code sc;
        sc = usart_write_buffer_wait(&g_wifi_uart_module, data, count);
        if (sc == STATUS_OK) {
                return count;
        }
        return -1;
}

static ssize_t wifi_write_stringv_wait (const char *data, va_list ap)
{
        int rc;
        int length;
        va_list vs;
        if (data == NULL) {
                return -1;
        }
        va_copy(vs, ap);
        length = vsnprintf(NULL, 0, data, vs);
        va_end(vs);
        if (length < 0) {
                return -1;
        }
        if (length + 1 >= SYNAPSE_WIFI_OUTPUT_BUFFER_SIZE) {
                synapse_errorf("string format: %s is too long: %d > %d", data, length, SYNAPSE_WIFI_OUTPUT_BUFFER_SIZE);
                return -1;
        }
        va_copy(vs, ap);
        rc = vsnprintf((char *) g_wifi_output_buffer, length + 1, data, vs);
        va_end(vs);
        if (rc < 0) {
                return -1;
        }
        return wifi_write_wait(g_wifi_output_buffer, strlen((char *) g_wifi_output_buffer));
}

static int wifi_inc_step (void)
{
        gettimeofday(&g_wifi_step_timeval, NULL);
        g_wifi_pstep  = g_wifi_step;
        g_wifi_step  += 1;
        return 0;
}

static int wifi_inc_operation (void)
{
        g_wifi_operation++;
        g_wifi_step  = 0;
        g_wifi_pstep = -1;
        gettimeofday(&g_wifi_operation_timeval, NULL);
        gettimeofday(&g_wifi_step_timeval, NULL);
        return 0;
}

static int wifi_set_state (unsigned int state)
{
        g_wifi_state             = state;
        g_wifi_state_status      = SYNAPSE_WIFI_STATE_STATUS_RUNNING;
        g_wifi_operation         = 0;
        g_wifi_step              = 0;
        g_wifi_pstep             = -1;
        gettimeofday(&g_wifi_state_timeval, NULL);
        gettimeofday(&g_wifi_operation_timeval, NULL);
        gettimeofday(&g_wifi_step_timeval, NULL);
        return 0;
}


static int wifi_wait_operation (uint32_t msec, int (*success) (void))
{
        int rc;
        struct timeval wait;
        struct timeval current;
        if (msec == 0) {
                return 1;
        }
        synapse_timeval_clear(&wait);
        wait.tv_sec = msec / 1000;
        wait.tv_usec = (msec % 1000) * 1000;
        synapse_timeval_add(&g_wifi_step_timeval, &wait, &wait);
        gettimeofday(&current, NULL);
        if (synapse_timeval_compare(&current, &wait, >)) {
                if (success) {
                        rc = success();
                        if (rc < 0) {
                                return -1;
                        }
                }
                return 1;
        }
        return 0;
}

static int wifi_end_command (uint32_t delay, int (*success) (void))
{
        int rc;
        rc = wifi_wait_operation(delay, NULL);
        if (rc > 0) {
                usart_abort_job(&g_wifi_uart_module, USART_TRANSCEIVER_RX);
                if (success) {
                        rc = success();
                        if (rc < 0) {
                                return -1;
                        }
                }
                return 1;
        } else if (rc < 0) {
                return -1;
        }
        return 0;
}

static int wifi_write_command (uint32_t delay, int (*success) (void), int (*error) (void), const char *command, ... )
{
        int rc;
        va_list ap;
        rc = wifi_wait_operation(delay, NULL);
        if (rc > 0) {
                usart_abort_job(&g_wifi_uart_module, USART_TRANSCEIVER_RX);
                memset(g_wifi_input_buffer, 0, SYNAPSE_WIFI_INPUT_BUFFER_SIZE);
                usart_read_buffer_job(&g_wifi_uart_module, g_wifi_input_buffer, SYNAPSE_WIFI_INPUT_BUFFER_SIZE - 1);
                va_start(ap, command);
                wifi_write_stringv_wait(command, ap);
                va_end(ap);
                if (success) {
                        rc = success();
                        if (rc < 0) {
                                return -1;
                        }
                }
                return 1;
        } else if (rc < 0) {
                if (error) {
                        rc = error();
                        if (rc < 0) {
                                return -1;
                        }
                }
                return -1;
        }
        return 0;
}

static int string_starts_with (const char *str, const char *prefix)
{
        int str_len = strlen(str);
        int prefix_len = strlen(prefix);
        return (str_len >= prefix_len) && (strncmp(str, prefix, prefix_len) == 0);
}

static int string_ends_with (const char *str, const char *suffix)
{
        int str_len = strlen(str);
        int suffix_len = strlen(suffix);
        return (str_len >= suffix_len) && (strcmp(str + (str_len - suffix_len), suffix) == 0);
}

static int wifi_wait_response_actual (int start, uint32_t duration, int (*success) (void), int (*error) (void), int (*timeout) (void), const char *response, ...)
{
        int rc;
        int ok;
        int err;
        //system_interrupt_enter_critical_section();
        //printf("g_wifi_input_buffer: '%s'\r\n", g_wifi_input_buffer);
        if (start == 1) {
                ok  = (strncmp((char *) &g_wifi_input_buffer, response, strlen(response)) == 0) ? 1 : 0;
        } else if (start == 2) {
                ok  = (strstr((char *) &g_wifi_input_buffer, response) != NULL) ? 1 : 0;
        } else {
                ok  = string_ends_with((char *) &g_wifi_input_buffer, response);
        }
        if (!ok) {
                err  = string_starts_with((char *) &g_wifi_input_buffer, "\r\nERROR\r\n");
                err |= string_starts_with((char *) &g_wifi_input_buffer, "\r\n\r\nERROR\r\n");
        }
        //system_interrupt_leave_critical_section();
        if (ok) {
                if (success) {
                        rc = success();
                        if (rc < 0) {
                                return -1;
                        }
                }
                return 1;
        } else if (err) {
                synapse_errorf("input error: '%s'\r\n", g_wifi_input_buffer);
                if (error) {
                        rc = error();
                        if (rc < 0) {
                                return -1;
                        }
                }
                return -1;
        } else if (wifi_wait_operation(duration, NULL)) {
                synapse_errorf("input timeout: '%s'\r\n", g_wifi_input_buffer);
                if (timeout) {
                        rc = timeout();
                        if (rc < 0) {
                                return -1;
                        }
                }
                return -2;
        }
        return 0;
}


static int wifi_wait_response_start (uint32_t duration, int (*success) (void), int (*error) (void), int (*timeout) (void), const char *response, ...)
{
        return wifi_wait_response_actual(1, duration, success, error, timeout, response);
}

static int wifi_wait_response_end (uint32_t duration, int (*success) (void), int (*error) (void), int (*timeout) (void), const char *response, ...)
{
        return wifi_wait_response_actual(0, duration, success, error, timeout, response);
}

static int wifi_operation_error (void)
{
        g_wifi_state_status = SYNAPSE_WIFI_STATE_STATUS_ERROR;
        return 0;
}

static int wifi_process_operation_atok_00 (void)
{
        wifi_inc_step();
        return 0;
}

static int wifi_process_operation_atok_01 (void)
{
        wifi_write_command(500, wifi_inc_step, wifi_operation_error, "AT+RESTORE\r\n");
        return 0;
}

static int wifi_process_operation_atok_02 (void)
{
        //wifi_wait_response_start(5000, wifi_inc_step, wifi_operation_error, wifi_operation_error, "AT\r\n\r\nOK\r\n");
	wifi_inc_step();
        return 0;
}

static int wifi_process_operation_atok_03 (void)
{
        wifi_end_command(500, wifi_inc_operation);
        return 0;
}

static struct wifi_step *g_wifi_operation_atok_steps[] = {
        &(struct wifi_step) { NULL, wifi_process_operation_atok_00 },
        &(struct wifi_step) { NULL, wifi_process_operation_atok_01 },
        &(struct wifi_step) { NULL, wifi_process_operation_atok_02 },
        &(struct wifi_step) { NULL, wifi_process_operation_atok_03 },
        NULL
};

static int wifi_process_operation_setup_00 (void)
{
        wifi_inc_step();
        return 0;
}

static int wifi_process_operation_setup_01 (void)
{
	wifi_write_command(500, wifi_inc_step, wifi_operation_error, "AT+RESTORE\r\n");
        return 0;
}

static int wifi_process_operation_setup_02 (void)
{
	wifi_wait_operation(1000, wifi_inc_step);
        return 0;
}

static int wifi_process_operation_setup_03 (void)
{
	wifi_write_command(500, wifi_inc_step, wifi_operation_error, "ATE0\r\n");
        return 0;
}

static int wifi_process_operation_setup_04 (void)
{
	wifi_wait_response_start(5000, wifi_inc_step, wifi_operation_error, wifi_operation_error, "ATE0\r\n\r\nOK\r\n");
        return 0;
}

static int wifi_process_operation_setup_05 (void)
{
	wifi_write_command(500, wifi_inc_step, wifi_operation_error, "AT+UART_CUR=115200,8,1,0,1\r\n");
        return 0;
}

static int wifi_process_operation_setup_06 (void)
{
	wifi_wait_response_start(5000, wifi_inc_step, wifi_operation_error, wifi_operation_error, "\r\nOK\r\n");
        return 0;
}

static int wifi_process_operation_setup_07 (void)
{
	wifi_write_command(500, wifi_inc_step, wifi_operation_error, "AT+CWMODE_CUR=1\r\n");
        return 0;
}

static int wifi_process_operation_setup_08 (void)
{
	wifi_wait_response_start(5000, wifi_inc_step, wifi_operation_error, wifi_operation_error, "\r\nOK\r\n");
        return 0;
}

static int wifi_process_operation_setup_09 (void)
{
	wifi_write_command(500, wifi_inc_step, wifi_operation_error, "AT+CWJAP_CUR=\"TTNET_ZyXEL_FVH9\",\"e481759A4B63c\"\r\n");
        return 0;
}

static int wifi_process_operation_setup_10 (void)
{
	wifi_wait_response_start(5000, wifi_inc_step, wifi_operation_error, wifi_operation_error, "WIFI");
        return 0;
}

static int wifi_process_operation_setup_11 (void)
{	
	wifi_wait_response_end(5000, wifi_inc_step, wifi_operation_error, wifi_operation_error, "OK\r\n");
        return 0;
}

static int wifi_process_operation_setup_12 (void)
{
	wifi_end_command(500, wifi_inc_operation);
        return 0;
}

static struct wifi_step *g_wifi_operation_setup_steps[] = {
        &(struct wifi_step) { NULL, wifi_process_operation_setup_00 },
        &(struct wifi_step) { NULL, wifi_process_operation_setup_01 },
        &(struct wifi_step) { NULL, wifi_process_operation_setup_02 },
        &(struct wifi_step) { NULL, wifi_process_operation_setup_03 },
        &(struct wifi_step) { NULL, wifi_process_operation_setup_04 },
        &(struct wifi_step) { NULL, wifi_process_operation_setup_05 },
        &(struct wifi_step) { NULL, wifi_process_operation_setup_06 },
        &(struct wifi_step) { NULL, wifi_process_operation_setup_07 },
        &(struct wifi_step) { NULL, wifi_process_operation_setup_08 },
        &(struct wifi_step) { NULL, wifi_process_operation_setup_09 },
        &(struct wifi_step) { NULL, wifi_process_operation_setup_10 },
        &(struct wifi_step) { NULL, wifi_process_operation_setup_11 },
        &(struct wifi_step) { NULL, wifi_process_operation_setup_12 },
        NULL
};
/*
static int wifi_process_operation_info_00 (void)
{
        wifi_inc_step();
        return 0;
}

static int wifi_process_operation_info_01 (void)
{
		wifi_write_command(500, wifi_inc_step, wifi_operation_error, "AT+CWDHCPS?\r\n");
		return 0;
}

static int wifi_process_operation_info_02 (void)
{
        wifi_wait_response_end(5000, wifi_inc_step, wifi_operation_error, wifi_operation_error, "\r\nOK\r\n");
        return 0;
}

static int wifi_process_operation_info_03 (void)
{
    	const char *beg;
    	const char *end;
    	const char *p_comma;

    	beg = (const char *) g_wifi_input_buffer;
    	end = strstr(beg, "\r\nOK\r\n");
    	if (end < beg) {
    			wifi_operation_error();
    	} else {
    			p_comma = strchr((const char*)g_wifi_input_buffer,',');
    			lease_width = (int) ( p_comma - (beg+strlen("+CWDHCPS:")) );
    			snprintf(g_wifi_lease_time, sizeof(g_wifi_lease_time), "%.*s", lease_width , beg+strlen("+CWDHCPS:") );
    			snprintf(g_wifi_ip_range, sizeof(g_wifi_ip_range), "%.*s", end - p_comma + 1, p_comma + 1);
    			synapse_infof("lease time: %s", g_wifi_lease_time);
    			synapse_infof("ip range  : %s", g_wifi_ip_range);
    			wifi_inc_step();
    	}
    	return 0;
}

static int wifi_process_operation_info_04 (void)
{
		wifi_end_command(500, wifi_inc_operation);
        return 0;
}

static struct wifi_step *g_wifi_operation_info_steps[] = {
        &(struct wifi_step) { NULL, wifi_process_operation_info_00 },
        &(struct wifi_step) { NULL, wifi_process_operation_info_01 },
        &(struct wifi_step) { NULL, wifi_process_operation_info_02 },
        &(struct wifi_step) { NULL, wifi_process_operation_info_03 },
        &(struct wifi_step) { NULL, wifi_process_operation_info_04 },
		NULL
};
*/

static int wifi_process_operation_connect_00 (void)
{
        wifi_inc_step();
        return 0;
}

static int wifi_process_operation_connect_01 (void)
{
	wifi_write_command(500, wifi_inc_step, wifi_operation_error, "AT+CIPSTART=\"TCP\",\"34.67.16.50\",12345\r\n");
	return 0;
}

static int wifi_process_operation_connect_02 (void)
{
        wifi_wait_response_end(5000, wifi_inc_step, wifi_operation_error, wifi_operation_error, "\r\nOK\r\n");
        return 0;
}

static int wifi_process_operation_connect_03 (void)
{
	g_wifi_state_tcp_request_emitter_callback(g_wifi_send_buffer);
	synapse_infof("gsm side buffer: %s",g_wifi_send_buffer);
	synapse_infof("gsm side buffer length: %d",strlen(g_wifi_send_buffer));
	//wifi_write_command(500, wifi_inc_step, wifi_operation_error, "AT+CIPSEND=%d\r\n",43);
	//wifi_write_command(500, wifi_inc_step, wifi_operation_error, "AT+CIPSEND=%d\r\n",strlen(try_http_get));
	wifi_write_command(500, wifi_inc_step, wifi_operation_error, "AT+CIPSEND=%d\r\n",strlen(g_wifi_send_buffer));
    	return 0;
}

static int wifi_process_operation_connect_04 (void)
{
        wifi_wait_response_start(5000, wifi_inc_step, wifi_operation_error, wifi_operation_error, "\r\nOK\r\n>");
        return 0;
}

static int wifi_process_operation_connect_05 (void)
{
	//wifi_write_command(500, wifi_inc_step, wifi_operation_error, "%s",try_http_get);
	wifi_write_command(500, wifi_inc_step, wifi_operation_error, "%s",g_wifi_send_buffer);
 	return 0;
}

static int wifi_process_operation_connect_06 (void)
{
	synapse_infof("gsm side buffer: %s",g_wifi_send_buffer);
	g_wifi_send_buffer[0]='\0';
	wifi_end_command(500, wifi_inc_operation);
        return 0;
}

static struct wifi_step *g_wifi_operation_connect_steps[] = {
        &(struct wifi_step) { NULL, wifi_process_operation_connect_00 },
        &(struct wifi_step) { NULL, wifi_process_operation_connect_01 },
        &(struct wifi_step) { NULL, wifi_process_operation_connect_02 },
        &(struct wifi_step) { NULL, wifi_process_operation_connect_03 },
        &(struct wifi_step) { NULL, wifi_process_operation_connect_04 },
        &(struct wifi_step) { NULL, wifi_process_operation_connect_05 },
        &(struct wifi_step) { NULL, wifi_process_operation_connect_06 },
		NULL
};

static int wifi_process_operation_success_00 (void)
{
        wifi_inc_step();
        return 0;
}

static int wifi_process_operation_success_01 (void)
{
        g_wifi_state_status = SYNAPSE_WIFI_STATE_STATUS_SUCCESS;
        wifi_inc_operation();
        return 0;
}

static struct wifi_step *g_wifi_operation_success_steps[] = {
        &(struct wifi_step) { NULL, wifi_process_operation_success_00 },
        &(struct wifi_step) { NULL, wifi_process_operation_success_01 },
        NULL
};

static struct wifi_operation *g_wifi_atok_operations[] = {
        &(struct wifi_operation) { "atok",        	 g_wifi_operation_atok_steps,   	          0 },
        &(struct wifi_operation) { "success",            g_wifi_operation_success_steps,                  0 },
        NULL
};

static struct wifi_operation *g_wifi_initial_operations[] = {
        &(struct wifi_operation) { "setup",          	 g_wifi_operation_setup_steps,                    0 },
        //&(struct wifi_operation) { "info",          	 g_wifi_operation_info_steps,			  0 },
	&(struct wifi_operation) { "success",            g_wifi_operation_success_steps,                  0 },
        NULL,
};

static struct wifi_operation *g_wifi_connect_operations[] = {
        &(struct wifi_operation) { "conncet",          	 g_wifi_operation_connect_steps,                  0 },
        //&(struct wifi_operation) { "info",          	 g_wifi_operation_info_steps,			  0 },
	&(struct wifi_operation) { "success",            g_wifi_operation_success_steps,                  0 },
        NULL,
};

static struct wifi_state g_wifi_states[] = {
        [SYNAPSE_WIFI_STATE_ATOK]          = { "atok",             g_wifi_atok_operations,                0 },
	[SYNAPSE_WIFI_STATE_INITIAL]       = { "initial",          g_wifi_initial_operations,             0 },
	[SYNAPSE_WIFI_STATE_CONNECT]       = { "connect",          g_wifi_connect_operations,             0 },
};

int synapse_wifi_init(void)
{
    unsigned int i;
    unsigned int j;
    unsigned int k;
    struct usart_config config_usart;

    memset(g_wifi_input_buffer, 0, SYNAPSE_WIFI_INPUT_BUFFER_SIZE);
    memset(&g_wifi_uart_module, 0, sizeof(struct usart_module));

    for (i = 0; i < (sizeof(g_wifi_states) / sizeof(g_wifi_states[0])); i++) {
            if (g_wifi_states[i].name == NULL) {
                    g_wifi_states[i].name = "";
            }

            g_wifi_states[i].noperations = 0;
            for (j = 0; g_wifi_states[i].operations[j]; j++) {
                    g_wifi_states[i].noperations += 1;

                    if (g_wifi_states[i].operations[j]->name == NULL) {
                            g_wifi_states[i].operations[j]->name = "";
                    }

                    g_wifi_states[i].operations[j]->nsteps = 0;
                    for (k = 0; g_wifi_states[i].operations[j]->steps[k]; k++) {
                            g_wifi_states[i].operations[j]->nsteps += 1;

                            if (g_wifi_states[i].operations[j]->steps[k]->name == NULL) {
                                    g_wifi_states[i].operations[j]->steps[k]->name = "";
                            }
                    }
            }
    }

    
    usart_get_config_defaults(&config_usart);
    config_usart.baudrate           = 115200;
    config_usart.generator_source   = GCLK_GENERATOR_0;
    config_usart.mux_setting        = USART_RX_1_TX_0_XCK_1;
    config_usart.pinmux_pad0        = PINMUX_PB08D_SERCOM4_PAD0;
    config_usart.pinmux_pad1        = PINMUX_PB09D_SERCOM4_PAD1;
    config_usart.pinmux_pad2        = PINMUX_UNUSED;
    config_usart.pinmux_pad3        = PINMUX_UNUSED;
    while (usart_init(&g_wifi_uart_module, SERCOM4, &config_usart) != STATUS_OK) {

    }
    usart_enable(&g_wifi_uart_module);

    synapse_wifi_atok();

	return 0;
}

int synapse_wifi_process(void)
{
    int rc;
    unsigned int nstates = sizeof(g_wifi_states) / sizeof(g_wifi_states[0]);

    if (g_wifi_state >= nstates) {
            synapse_errorf("state: %d is out of range: %d", g_wifi_state, nstates);
            return -1;
    }

    if (g_wifi_state_status != SYNAPSE_WIFI_STATE_STATUS_RUNNING) {
            return g_wifi_state_status;
    }

    if (g_wifi_operation >= g_wifi_states[g_wifi_state].noperations) {
            synapse_errorf("state: %d, %s operation: %d is out of range: %d",
                            g_wifi_state, wifi_state_string(),
                            g_wifi_operation, g_wifi_states[g_wifi_state].noperations);
            return -1;
    }

    if (g_wifi_step >= g_wifi_states[g_wifi_state].operations[g_wifi_operation]->nsteps) {
            synapse_errorf("state: %d, %s operation: %d, %s step: %d is out of range: %d",
                            g_wifi_state, wifi_state_string(),
                            g_wifi_operation, g_wifi_states[g_wifi_state].operations[g_wifi_operation]->name,
                            g_wifi_step, g_wifi_states[g_wifi_state].operations[g_wifi_operation]->nsteps);
            return -1;
    }

    if (g_wifi_step != g_wifi_pstep) {
            if (g_wifi_step == 0) {
                    synapse_infof("wifi: %s:%s:%s",
                                    wifi_state_string(),
                                    g_wifi_states[g_wifi_state].operations[g_wifi_operation]->name,
                                    g_wifi_states[g_wifi_state].operations[g_wifi_operation]->steps[g_wifi_step]->name);
            } else {
                    synapse_infof("wifi: %s:%s:%d/%d:%s",
                                    wifi_state_string(),
                                    g_wifi_states[g_wifi_state].operations[g_wifi_operation]->name,
                                    g_wifi_step, g_wifi_states[g_wifi_state].operations[g_wifi_operation]->nsteps,
                                    g_wifi_states[g_wifi_state].operations[g_wifi_operation]->steps[g_wifi_step]->name);
            }
            g_wifi_pstep = g_wifi_step;
    }

    rc = g_wifi_states[g_wifi_state].operations[g_wifi_operation]->steps[g_wifi_step]->process();
    if (rc < 0) {
            return -1;
    }

    return g_wifi_state_status;
}

void synapse_wifi_uninit(void)
{

}

void buffer_copier_caller(void (*emitter)(char *buffer)){
	g_wifi_state_tcp_request_emitter_callback        = emitter;
}

const char * wifi_state_string (void)
{
        switch (g_wifi_state) {
                case SYNAPSE_WIFI_STATE_ATOK:  	           return "atok";
                //case SYNAPSE_WIFI_STATE_SHUTDOWN:          return "shutdown";
                case SYNAPSE_WIFI_STATE_INITIAL:           return "initial";
                case SYNAPSE_WIFI_STATE_CONNECT:           return "connect";
        }
        return "unknown";
}
/*
int synapse_wifi_shutdown (void)
{
        return wifi_set_state(SYNAPSE_WIFI_STATE_SHUTDOWN);
}
*/
int synapse_wifi_atok (void)
{
        return wifi_set_state(SYNAPSE_WIFI_STATE_ATOK);
}

int synapse_wifi_initial (void)
{
        return wifi_set_state(SYNAPSE_WIFI_STATE_INITIAL);
}

int synapse_wifi_connect (void)
{
        return wifi_set_state(SYNAPSE_WIFI_STATE_CONNECT);
}

unsigned int synapse_wifi_get_state (void)
{
        return g_wifi_state;
}

unsigned int synapse_wifi_get_state_status (void)
{
        return g_wifi_state_status;
}
