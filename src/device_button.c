
#include <stdint.h>

#include <asf.h>
#include <common_pinmux.h>

#include "device_button.h"

int synapse_button_init (void)
{
        synapse_pinmux_setup(PIN_BUTTON, 0, 0, 0);
        return 0;
}

void synapse_button_uninit (void)
{

}

int synapse_button_process (void)
{
        return 0;
}

int synapse_button_get_pressed (void)
{
        return !synapse_pinmux_get_level(PIN_BUTTON);
}
