
int synapse_sht30_init (void);
void synapse_sht30_uninit (void);
int synapse_sht30_process (void);

int sht30_read_status (uint16_t *Result);
int synapse_sht30_get_temperature_and_humidity_single_shot (int32_t *result_temp, int32_t *result_hum);

//int synapse_sht30_get_temperature (int32_t *result);
//int synapse_sht30_get_humidity (int32_t *result);
